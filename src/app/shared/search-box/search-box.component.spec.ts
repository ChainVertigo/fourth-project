import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBoxComponent } from './search-box.component';
import { Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { SharedModule } from '../shared.module';
import { SearchService } from 'src/app/core/services/search.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RouterTestingModule } from '@angular/router/testing';
import { RecipesComponent } from 'src/app/recipes/recipes.component';
import { RecipeViewComponent } from 'src/app/recipes/recipe-view/recipe-view.component';
import { TotalNutritionComponent } from 'src/app/recipes/total-nutrition/total-nutrition.component';
import { TimeAgoPipe } from 'time-ago-pipe';
import { ItemNutritionComponent } from 'src/app/recipes/item-nutrition/item-nutrition.component';
import { AddContentsComponent } from 'src/app/recipes/add-contents/add-contents.component';
import { CreateRecipeComponent } from 'src/app/recipes/create-recipe/create-recipe.component';
import { RecipesRoutingModule } from 'src/app/recipes/recipes-routing.module';
import { RecipeDetailsComponent } from 'src/app/recipes/recipe-details/recipe-details.component';
import { AngularStickyThingsModule } from '@w11k/angular-sticky-things';

describe('SearchBoxComponent', () => {
  let component: SearchBoxComponent;
  let fixture: ComponentFixture<SearchBoxComponent>;
  let router: Router;

  const searchService = jasmine.createSpyObj('SearchService', ['search$', 'emitSearch']);
  const modalService = jasmine.createSpyObj('NgbModal', ['open']);
  const activatedRoute = jasmine.createSpyObj('ActivatedRoute', ['']);
  const location = jasmine.createSpyObj('Location', ['go']);

  // Mock Data
  const nutrition = {
    PROCNT: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FAT: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    CHOCDF: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    ENERC_KCAL: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    SUGAR: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FIBTG: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    CA: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FE: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    P: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    K: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    NA: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    VITA_IU: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    TOCPHA: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    VITD: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    VITC: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    VITB12: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FOLAC: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    CHOLE: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FATRN: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FASAT: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FAMS: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FAPU: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
  };

  // Mock Data
  activatedRoute.data = of({
    recipes: {
      recipes: [{
        id: 'ab5035c0-f411-4868-b20e-e068ac88bbf9',
        title: 'Recipe',
        description: 'Content 1',
        category: 'something',
        products: [],
        recipes: [],
        subrecipes: [],
        nutrition,
        user: 'ivo',
        userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
        created: new Date(),
      }],
      count: 26,
    },
    categories: [
      { name: 'Appetizers' },
      { name: 'Desserts' },
      { name: 'Main' },
      { name: 'Salads' },
      { name: 'Sides' },
      { name: 'Soups' },
    ],
  });

  // Mock Data
  activatedRoute.queryParamMap = of({
    params: {
      page: 1,
    },
  });

  // Mock Data
  searchService.search$ = of('clearTheSearch');
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RecipesComponent,
        RecipeViewComponent,
        RecipeDetailsComponent,
        CreateRecipeComponent,
        AddContentsComponent,
        ItemNutritionComponent,
        RecipeViewComponent,
        TimeAgoPipe,
        TotalNutritionComponent,
      ],
      imports: [
        SharedModule,
        RouterTestingModule,
        RecipesRoutingModule,
        AngularStickyThingsModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: activatedRoute,
        },
        {
          provide: Router,
          useValue: router,
        },
        {
          provide: SearchService,
          useValue: searchService,
        },
        {
          provide: NgbModal,
          useValue: modalService,
        },
        {
          provide: Location,
          useValue: location,
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize data with correct categories', () => {
    expect(component.categories[0].name).toBe('Appetizers', );
    expect(component.categories[1].name).toBe('Desserts', );
    expect(component.categories[2].name).toBe('Main', );
    expect(component.categories[3].name).toBe('Salads', );
    expect(component.categories[4].name).toBe('Sides', );
    expect(component.categories[5].name).toBe('Soups', );
  });

  it('should initialize clearSearch to be clearTheSearch', () => {
    expect(component.clearSearchDisabled).toBe(true);
  });

  it('open should call modalService.open once', () => {
    modalService.open.calls.reset();

    component.open('modalWindow');

    expect(modalService.open).toHaveBeenCalledTimes(1);
  });
});
