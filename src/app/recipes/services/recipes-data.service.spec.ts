import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { RecipesDataService } from './recipes-data.service';
import { of } from 'rxjs';
import { CreateUpdateRecipe } from '../../../app/common/interfaces/create-update-recipe';

describe('RecipesDataService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

  // Mock Data
  const nutrition = {
    PROCNT: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FAT: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    CHOCDF: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    ENERC_KCAL: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    SUGAR: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FIBTG: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    CA: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FE: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    P: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    K: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    NA: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    VITA_IU: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    TOCPHA: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    VITD: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    VITC: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    VITB12: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FOLAC: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    CHOLE: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FATRN: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FASAT: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FAMS: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
    FAPU: {
      description: 'eee',
      unit: 'eee',
      value: 1,
    },
  };

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: HttpClient,
        useValue: http,
      },
    ],
  }));

  it('should be created', () => {
    const service: RecipesDataService = TestBed.get(RecipesDataService);
    expect(service).toBeTruthy();
  });

  it('getRecipes should return the correct data', () => {
    const service: RecipesDataService = TestBed.get(RecipesDataService);

    http.get.and.returnValue(of(
      {
        recipes: [{
          id: 'test-id',
          title: 'Test Recipe',
          description: 'Test Content 1',
          category: 'something',
          products: [],
          recipes: [],
          subrecipes: [],
          nutrition,
          user: 'ivo',
          userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
          created: new Date(),
        }],
        count: 13,
      }
    ));

    service.getRecipes().subscribe(
      (data) => {
        expect(data.recipes[0].id).toBe('test-id');
        expect(data.count).toBe(13);
      },
    );
  });

  it('getRecipes should call http.get one time', () => {
    const service: RecipesDataService = TestBed.get(RecipesDataService);

    http.get.calls.reset();

    service.getRecipes().subscribe(
      (data) => expect(http.get).toHaveBeenCalledTimes(1)
    );
  });

  it('getSingleRecipe should return a single recipe', () => {
    const service: RecipesDataService = TestBed.get(RecipesDataService);

    http.get.and.returnValue(of(
      {
        id: 'test-id',
        title: 'Test Recipe',
        description: 'Test Content 1',
        category: 'something',
        products: [],
        recipes: [],
        subrecipes: [],
        nutrition,
        user: 'ivo',
        userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
        created: new Date(),
      },
    ));

    service.getSingleRecipe('test-id').subscribe(
      (recipe) => expect(recipe.title).toBe('Test Recipe')
    );
  });

  it('createRecipe should return the created recipe', () => {
    const service: RecipesDataService = TestBed.get(RecipesDataService);

    http.post.and.returnValue(of(
      {
        id: 'new-recipe-id',
        title: 'New Recipe',
        description: 'Recipe Description',
        category: 'Soups',
        products: [],
        recipes: [],
        subrecipes: [],
        nutrition,
        user: 'ivo',
        userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
        created: new Date(),
      },
    ));

    const recipeToCreate: CreateUpdateRecipe = {
      title: 'New Recipe',
      description: 'Recipe Description',
      category: { name: 'Soups' },
      products: [],
      recipes: [],
      nutrition,
    };

    service.createRecipe(recipeToCreate).subscribe(
      (recipe) => expect(recipe.title).toBe('New Recipe')
    );
  });

  it('updateRecipe should return the updated recipe', () => {
    const service: RecipesDataService = TestBed.get(RecipesDataService);

    http.put.and.returnValue(of(
      {
        id: 'updated-recipe-id',
        title: 'Updated Recipe',
        description: 'Updated Recipe Description',
        category: 'Soups',
        products: [],
        recipes: [],
        subrecipes: [],
        nutrition,
        user: 'ivo',
        userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
        created: new Date(),
      },
    ));

    const recipeToUpdate: CreateUpdateRecipe = {
      id: 'updated-recipe-id',
      title: 'Updated Recipe',
      description: 'Updated Recipe Description',
      category: { name: 'Soups' },
      products: [],
      recipes: [],
      nutrition,
    };

    service.updateRecipe(recipeToUpdate).subscribe(
      (recipe) => expect(recipe.title).toBe('Updated Recipe')
    );
  });

  it('deleteRecipe should return the deleted recipe', () => {
    const service: RecipesDataService = TestBed.get(RecipesDataService);

    http.delete.and.returnValue(of(
      {
        id: 'deleted-recipe-id',
        title: 'Deleted Recipe',
        description: 'Deleted Recipe Description',
        category: 'Soups',
        products: [],
        recipes: [],
        subrecipes: [],
        nutrition,
        user: 'ivo',
        userID: '4cc8b197-cd03-4abf-9997-5f7a3a292211',
        created: new Date(),
      },
    ));

    service.deleteRecipe('deleted-recipe-id').subscribe(
      (recipe) => expect(recipe.title).toBe('Deleted Recipe')
    );
  });
});
