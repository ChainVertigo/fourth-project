import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { ProductsDataService } from './products-data.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { request } from 'https';

describe('ProductsDataService', () => {

  const http = jasmine.createSpyObj('HttpClient', ['get']);
  let backend: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: HttpClient,
        useValue: http,
      }
    ],
    imports: [
      HttpClientTestingModule,
    ]
  }));

  beforeEach(() => {
    backend = TestBed.get(HttpTestingController);
  })
  it('should be created', () => {
    const service: ProductsDataService = TestBed.get(ProductsDataService);
    expect(service).toBeTruthy();
  });

  it('getProduct should return array of products', () => {
    const service: ProductsDataService = TestBed.get(ProductsDataService);
    http.get.and.returnValue(of([
      { description: 'desc', foodGroupCode: 100 },
      { description: 'asdf', foodGroupCode: 300 },
      { description: 'another', foodGroupCode: 700 },
    ]
    ));
    service.getProducts().subscribe(
      (res) => {
        expect(res[0].description).toBe('desc');
        expect(res[1].description).toBe('asdf');
        expect(res[2].description).toBe('another');
        expect(res[0].foodGroupCode).toBe(100);
        expect(res[1].foodGroupCode).toBe(300);
        expect(res[2].foodGroupCode).toBe(700);
      });
  });

  it('getProducts with params should math default params url', () => {
    const service: ProductsDataService = TestBed.get(ProductsDataService);
    service.getProducts().subscribe();
    const calls = backend.match((request) => {
      return request.url == `http://localhost:3000/api/products` &&
        request.urlWithParams == `http://localhost:3000/api/products?page=1&search=`
        && request.method === 'GET';
    });
    backend.expectNone(`http://localhost:3000/api/products?page=1&search=`);
    backend.verify();
  });

  it('getProducts with params should math custom params url', () => {
    const service: ProductsDataService = TestBed.get(ProductsDataService);
    service.getProducts(3, 'butter').subscribe();
    const calls = backend.match((request) => {
      return request.url == `http://localhost:3000/api/products` &&
        request.urlWithParams == `http://localhost:3000/api/products?page=3&search=butter`
        && request.method === 'GET';
    });
    backend.expectNone(`http://localhost:3000/api/products?page=3&search=butter`);
    backend.verify();
  });

  it('getProducts with params should math custom params url', () => {
    const service: ProductsDataService = TestBed.get(ProductsDataService);
    service.getProducts(4, '', 100).subscribe();
    const calls = backend.match((request) => {
      return request.url == `http://localhost:3000/api/products` &&
        request.urlWithParams == `http://localhost:3000/api/products?page=4&search=&foodGroup=100`
        && request.method === 'GET';
    });
    backend.expectNone(`http://localhost:3000/api/products?page=4&search=&foodGroup=100`);
    backend.verify();
  });

  it('getFoodGroups should return food groups', () => {
    const service: ProductsDataService = TestBed.get(ProductsDataService);
    http.get.and.returnValue(of([
      { description: 'desc', code: 100 },
      { description: 'asdf', code: 300 },
      { description: 'another', code: 700 },
    ]
    ));
    service.getFoodGroups().subscribe(
      (res) => {
        expect(res[0].description).toBe('desc');
        expect(res[1].description).toBe('asdf');
        expect(res[2].description).toBe('another');
        expect(res[0].code).toBe(100);
        expect(res[1].code).toBe(300);
        expect(res[2].code).toBe(700);
      });
  });

});
